import Button from "@material-ui/core/Button"
import Divider from "@material-ui/core/Divider"
import { makeStyles } from "@material-ui/core/styles"
import React from "react"
import Jumbotron from "react-bootstrap/Jumbotron"

import { signInWithGoogle } from "../services/firebase"

const useStyles = makeStyles({
  divider: {
    marginBottom: "1.25rem",
  },
  jumbotron: {
    height: "100vh",
    backgroundColor: "#fff !important",
  },
  jumbotronTitle: {
    fontFamily: "'Cinzel Decorative', cursive",
  },
  jumbotronContent: {
    maxWidth: 1200,
    margin: "auto",
  },
  button: {
    maxWidth: 300,
    width: "calc(50% - 0.5rem)",
  },
  lastButton: {
    marginLeft: "1rem",
  },
})

function HomePresentation() {
  const classes = useStyles()

  return (
    <>
      <Jumbotron className={classes.jumbotron}>
        <div className={classes.jumbotronContent}>
          <h6>Mesurez les relations entre les produits&nbsp;!</h6>
          <h1 className={classes.jumbotronTitle}>LabelApp</h1>
          <Divider className={classes.divider} />
          <p>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={signInWithGoogle}
            >
              Labéliser
            </Button>
            <Button
              className={`${classes.button} ${classes.lastButton}`}
              variant="outlined"
              color="primary"
            >
              <a href="/explore.html">Graphe de similarité</a>
            </Button>
          </p>
        </div>
      </Jumbotron>
    </>
  )
}

export default HomePresentation
