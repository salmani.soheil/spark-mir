import { db } from "../services/firebase"
import { doc, setDoc, collection } from "firebase/firestore"

const saveLabellings = (user, barcode, matches) => {
  const usersRef = collection(db, "labellings")
  console.log(user.email)
  console.log(barcode)
  let data = {}
  data[barcode] = matches.map(match => ({
    barcode: match.barcode,
    labelling: match.labelling,
  }))
  setDoc(doc(usersRef, user.email), data, { merge: true })
}

export default saveLabellings
