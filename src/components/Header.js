import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"

import "/static/carrefour-google-lab-logo.css"

const useStyles = makeStyles({
  header: {
    marginBottom: "1rem",
    minWidth: 540,
  },
  appBar: {
    padding: "0.75rem 0 0.75rem 0",
  },
  siteName: {
    fontFamily: "'Cinzel Decorative', cursive",
    fontSize: "1.5rem",
    margin: "1rem 0 1rem 0",
    flexGrow: 1,
  },
  logo: {
    display: "flex",
    width: 300,
    justifyContent: "space-between",
    alignItems: "center",
  },
})

export default () => {
  const classes = useStyles()

  return (
    <header className={classes.header}>
      <AppBar className={classes.appBar} position="static">
        <Toolbar>
          <Typography className={classes.siteName}>LabelApp</Typography>
          <Typography variant="overline" className={classes.logo}>
            Powered by
            <svg
              version="1.0"
              xmlns="http://www.w3.org/2000/svg"
              width="200px"
              viewBox="-100 -10 1750 560"
              preserveAspectRatio="xMidYMid meet"
            >
              <defs>
                <path
                  id="square"
                  d="M 0 0 l 90 -90 90 90 -90 90 z"
                  stroke="black"
                  stroke-width="5"
                />
              </defs>
              <g
                class="logo-text"
                transform="translate(-450, 575) scale(0.100000, -0.100000)"
                fill="#000"
              >
                <path
                  d="M11788 4480 c-361 -66 -581 -325 -581 -685 0 -418 315 -707 750 -689
193 8 356 91 467 238 67 89 72 78 -66 147 -68 34 -124 57 -126 51 -2 -6 -16
-27 -31 -46 -202 -254 -599 -152 -687 177 -28 105 -14 240 36 342 90 187 338
277 519 189 54 -27 123 -87 153 -134 l16 -25 121 60 c66 32 121 62 121 66 0
14 -90 127 -128 161 -56 51 -151 104 -229 128 -76 24 -254 35 -335 20z"
                />
                <path
                  d="M17245 4473 c-134 -48 -205 -156 -205 -310 l0 -63 -85 0 -85 0 0
-110 0 -109 83 -3 82 -3 3 -372 2 -373 125 0 125 0 0 375 0 375 100 0 100 0 0
110 0 110 -100 0 -100 0 0 40 c0 57 23 106 62 130 41 25 83 25 131 1 l38 -19
46 70 c55 82 54 90 -30 132 -76 38 -214 47 -292 19z"
                />
                <path
                  d="M13030 4105 c-71 -19 -210 -88 -235 -115 -19 -21 -19 -22 19 -93 56
-104 51 -101 101 -62 68 51 142 75 233 74 159 -1 222 -60 222 -206 0 -40 -2
-73 -5 -73 -3 0 -16 10 -28 21 -64 62 -232 101 -347 81 -118 -21 -194 -74
-241 -168 -20 -41 -24 -64 -24 -139 0 -75 4 -98 24 -139 34 -67 82 -113 160
-149 58 -27 77 -31 151 -31 103 0 180 22 249 69 28 19 53 35 56 35 3 0 5 -18
5 -40 l0 -40 128 0 127 0 -2 347 c-2 213 -7 366 -14 395 -23 97 -83 163 -194
214 -54 25 -73 28 -195 31 -97 2 -150 -1 -190 -12z m248 -561 c20 -8 49 -27
65 -41 23 -23 27 -35 27 -83 0 -48 -4 -60 -27 -82 -38 -35 -59 -44 -126 -57
-85 -15 -151 1 -199 49 -34 34 -38 43 -38 88 0 43 5 57 31 87 55 62 171 80
267 39z"
                />
                <path
                  d="M14480 4111 c-47 -15 -115 -53 -162 -92 -26 -21 -48 -39 -50 -39 -2
0 -3 26 -3 57 l0 58 -127 3 -128 3 0 -486 0 -485 130 0 130 0 0 321 c-1 320
-1 322 21 345 41 44 127 77 212 82 l77 5 0 118 0 119 -37 -1 c-21 0 -49 -4
-63 -8z"
                />
                <path
                  d="M15313 4100 c-58 -20 -101 -47 -150 -94 l-33 -30 0 62 0 62 -130 0
-130 0 0 -485 0 -485 130 0 130 0 0 324 0 324 33 28 c52 44 115 67 200 72 l77
5 0 118 0 119 -37 0 c-21 -1 -62 -9 -90 -20z"
                />
                <path
                  d="M16019 4102 c-89 -30 -157 -72 -214 -134 -98 -106 -141 -231 -132
-388 19 -346 307 -539 682 -458 56 12 178 66 213 95 l24 19 -57 82 -56 81 -47
-30 c-101 -64 -243 -81 -340 -39 -56 24 -116 83 -138 136 -29 68 -52 64 346
64 l360 0 0 68 c-1 143 -50 282 -136 380 -33 38 -68 63 -131 94 -85 42 -88 43
-202 45 -91 2 -128 -1 -172 -15z m287 -217 c37 -23 56 -44 77 -84 15 -30 27
-65 27 -78 l0 -23 -234 0 c-129 0 -237 4 -240 9 -9 14 20 87 49 126 15 19 50
46 78 60 44 22 62 26 123 23 58 -3 80 -9 120 -33z"
                />
                <path
                  d="M18073 4105 c-158 -44 -278 -155 -335 -311 -31 -85 -31 -273 0 -358
33 -89 65 -137 131 -199 104 -96 225 -137 381 -131 219 8 396 131 461 321 20
58 24 87 24 193 -1 106 -4 134 -24 183 -54 137 -152 235 -288 287 -75 30 -270
38 -350 15z m267 -235 c158 -81 183 -357 42 -480 -119 -106 -295 -71 -369 74
-38 74 -43 188 -13 270 54 142 207 204 340 136z"
                />
                <path
                  d="M20796 4095 c-32 -13 -85 -44 -117 -69 l-59 -46 0 60 0 60 -130 0
-130 0 0 -485 0 -485 130 0 130 0 0 320 0 321 25 25 c44 44 132 78 213 82 l72
4 0 119 0 119 -37 0 c-21 0 -64 -11 -97 -25z"
                />
                <path
                  d="M19062 3708 c3 -389 3 -394 27 -446 48 -106 134 -154 280 -156 119
-1 203 25 291 90 l60 44 0 -55 0 -55 130 0 130 0 0 485 0 485 -130 0 -130 0 0
-334 0 -334 -31 -27 c-85 -71 -199 -94 -283 -57 -37 17 -51 30 -66 64 -18 40
-20 67 -20 366 l0 322 -131 0 -130 0 3 -392z"
                />
                <path
                  d="M18192 2138 l3 -673 365 0 365 0 0 30 0 30 -335 2 -335 2 -3 641 -2
640 -30 0 -30 0 2 -672z"
                />
                <path
                  d="M20092 2138 l3 -673 25 0 c24 0 25 3 28 73 2 39 5 72 7 72 3 0 20
-17 39 -37 46 -51 128 -99 201 -118 242 -62 470 86 524 343 16 78 14 253 -5
325 -35 136 -126 252 -237 303 -56 26 -72 29 -173 29 -107 0 -112 -1 -186 -37
-46 -23 -91 -55 -117 -83 -23 -25 -44 -45 -46 -45 -3 0 -5 117 -5 260 l0 260
-30 0 -30 0 2 -672z m562 233 c39 -19 80 -49 101 -74 160 -186 149 -546 -21
-707 -59 -56 -99 -76 -180 -91 -113 -21 -270 37 -359 132 l-45 48 0 271 0 271
47 50 c126 134 307 174 457 100z"
                />
                <path
                  d="M11729 2760 c-209 -52 -365 -173 -453 -352 -47 -94 -67 -194 -67
-327 1 -396 293 -680 706 -684 209 -2 387 66 528 202 l47 45 0 258 0 258 -320
0 -321 0 3 -122 3 -123 178 -3 177 -2 0 -80 0 -79 -45 -27 c-90 -52 -143 -67
-245 -67 -111 0 -175 20 -254 80 -207 158 -223 501 -31 682 30 29 81 64 112
79 52 24 70 27 168 27 105 0 113 -2 170 -32 61 -32 96 -60 134 -109 l21 -27
53 28 c142 76 177 98 177 111 0 8 -37 52 -82 98 -94 94 -186 146 -305 171 -95
20 -260 18 -354 -5z"
                />
                <path d="M16410 2090 l0 -670 130 0 130 0 0 670 0 670 -130 0 -130 0 0 -670z" />
                <path
                  d="M19319 2447 c-59 -17 -129 -58 -181 -108 l-42 -40 22 -21 21 -21 43
39 c57 53 102 79 167 98 112 33 232 12 303 -52 66 -60 78 -96 78 -249 l0 -133
-60 44 c-87 64 -144 81 -270 80 -121 0 -170 -17 -240 -80 -70 -64 -93 -114
-98 -215 -5 -108 13 -169 69 -233 131 -148 412 -153 561 -9 l37 36 3 -59 c3
-56 4 -59 31 -62 l27 -3 0 383 c0 220 -4 398 -10 418 -21 75 -64 126 -136 161
-63 31 -77 34 -174 36 -63 2 -123 -2 -151 -10z m164 -417 c76 -12 154 -48 206
-95 l41 -37 0 -129 0 -128 -35 -36 c-74 -73 -223 -124 -323 -109 -70 10 -147
50 -187 97 -37 43 -65 118 -65 172 0 91 63 196 142 236 69 35 133 44 221 29z"
                />
                <path
                  d="M13145 2413 c-267 -35 -439 -236 -438 -513 1 -241 140 -426 368 -486
68 -18 227 -18 300 1 268 70 419 357 339 645 -68 241 -304 388 -569 353z m184
-249 c189 -95 187 -416 -4 -517 -56 -30 -155 -29 -211 1 -115 60 -171 216
-129 358 46 155 204 228 344 158z"
                />
                <path
                  d="M14315 2405 c-179 -32 -315 -151 -376 -326 -31 -90 -32 -237 -2 -334
52 -167 175 -287 338 -330 74 -19 233 -19 304 0 177 47 310 184 351 364 7 30
13 92 12 137 -1 251 -161 447 -400 490 -93 16 -135 16 -227 -1z m210 -234 c53
-24 97 -68 126 -128 33 -69 34 -197 1 -268 -46 -101 -117 -148 -222 -149 -73
0 -126 21 -171 71 -92 98 -103 275 -26 390 62 92 193 129 292 84z"
                />
                <path
                  d="M15462 2410 c-120 -25 -227 -107 -281 -212 -76 -152 -79 -388 -5
-544 114 -242 443 -304 638 -120 l36 33 0 -72 c0 -82 -12 -125 -48 -174 -54
-73 -198 -107 -325 -77 -80 19 -114 35 -162 78 -20 17 -37 29 -39 27 -13 -13
-106 -170 -106 -178 0 -28 149 -109 240 -132 79 -20 279 -17 360 4 136 36 241
111 289 205 47 93 51 135 51 655 l0 487 -130 0 -130 0 0 -56 0 -56 -50 46
c-28 24 -76 54 -106 65 -64 24 -171 34 -232 21z m260 -236 c25 -8 64 -31 87
-50 l41 -36 0 -168 0 -168 -32 -28 c-55 -46 -116 -68 -188 -67 -105 0 -192 59
-225 153 -21 58 -21 176 0 226 53 128 182 184 317 138z"
                />
                <path
                  d="M17303 2406 c-236 -54 -386 -249 -386 -501 0 -250 143 -437 378 -495
80 -19 231 -15 322 10 81 22 189 75 206 101 7 11 -4 34 -39 84 -26 39 -50 74
-54 78 -4 5 -31 -6 -60 -23 -201 -118 -425 -55 -484 138 l-7 22 360 0 361 0 0
83 c0 234 -121 421 -314 487 -79 27 -204 34 -283 16z m232 -214 c54 -29 125
-133 125 -183 0 -5 -108 -9 -240 -9 -270 0 -257 -4 -221 76 26 56 86 112 138
127 48 14 164 8 198 -11z"
                />
              </g>
              <g transform="scale(1, -1) translate(30, -5)" fill="black">
                <use href="#square" x="0" y="-180" class="square-1" />
                <use href="#square" x="90" y="-90" class="square-2" />
                <use href="#square" x="180" y="-180" class="square-3" />
                <use href="#square" x="270" y="-90" class="square-4" />
                <use href="#square" x="360" y="-180" class="square-5" />
                <use href="#square" x="450" y="-270" class="square-6" />
                <use href="#square" x="360" y="-360" class="square-7" />
                <use href="#square" x="270" y="-450" class="square-8" />
                <use href="#square" x="180" y="-360" class="square-9" />
                <use href="#square" x="90" y="-450" class="square-10" />
                <use href="#square" x="0" y="-360" class="square-11" />
                <use href="#square" x="-90" y="-270" class="square-12" />
              </g>
              <g transform="scale(1, -1)" fill="rgba(255, 0, 0, 0.8)">
                <use href="#square" x="0" y="-180" class="square-1" />
                <use href="#square" x="90" y="-90" class="square-2" />
                <use href="#square" x="180" y="-180" class="square-3" />
                <use
                  href="#square"
                  x="270"
                  y="-90"
                  fill="rgba(30, 144, 255, 0.8)"
                  class="square-4"
                />
                <use
                  href="#square"
                  x="360"
                  y="-180"
                  fill="rgba(30, 144, 255, 0.8)"
                  class="square-5"
                />
                <use
                  href="#square"
                  x="450"
                  y="-270"
                  fill="rgba(30, 144, 255, 0.8)"
                  class="square-6"
                />
                <use
                  href="#square"
                  x="360"
                  y="-360"
                  fill="rgba(30, 144, 255, 0.8)"
                  class="square-7"
                />
                <use
                  href="#square"
                  x="270"
                  y="-450"
                  fill="rgba(30, 144, 255, 0.8)"
                  class="square-8"
                />
                <use
                  href="#square"
                  x="180"
                  y="-360"
                  fill="rgba(30, 144, 255, 0.8)"
                  class="square-9"
                />
                <use href="#square" x="90" y="-450" class="square-10" />
                <use href="#square" x="0" y="-360" class="square-11" />
                <use href="#square" x="-90" y="-270" class="square-12" />
              </g>
            </svg>
          </Typography>
        </Toolbar>
      </AppBar>
    </header>
  )
}
