import React, { useContext, useEffect, useState } from "react"
import Grid from "@material-ui/core/Grid"

import CurrentProduct from "../components/CurrentProduct"
import InstructionsDialog from "../components/InstructionsDialog"
import MatchingProducts from "../components/MatchingProducts"
import UserCard from "../components/UserCard"
import saveLabellings from "../hooks/save-labellings"
import useProductMatches from "../hooks/use-product-matches"
import useProducts from "../hooks/use-products"
import { UserContext } from "../providers/UserProvider"

function LabelApp() {
  const user = useContext(UserContext)
  const products = useProducts()
  const productMatches = useProductMatches()

  const [currentProductIndex, setCurrentProductIndex] = useState(0)
  const [currentProduct, setCurrentProduct] = useState(null)
  const [currentProductMatches, setCurrentProductMatches] = useState(null)

  useEffect(() => {
    setCurrentProduct(
      products.find(product => product.barcode === productMatches[0].barcode)
    )
  }, [])

  useEffect(() => {
    setCurrentProduct(
      products.find(
        product =>
          product.barcode === productMatches[currentProductIndex].barcode
      )
    )
  }, [currentProductIndex])

  useEffect(() => {
    if (!currentProduct) {
      return
    }
    setCurrentProductMatches(
      productMatches
        .find(obj => obj.barcode === currentProduct.barcode)
        .matches.map(match => {
          return {
            barcode: match.barcode,
            description: products.find(
              product => product.barcode === match.barcode
            ).description,
            packaging: products.find(
              product => product.barcode === match.barcode
            ).packaging,
            photos: products.find(product => product.barcode === match.barcode)
              .photos,
            similarityScore: match.similarityScore,
            labelling: "AUCUN",
          }
        })
    )
  }, [currentProduct])

  const handleNextProduct = () => {
    saveLabellings(user, currentProduct.barcode, currentProductMatches)
    setCurrentProductIndex(currentProductIndex + 1)
  }

  if (currentProduct && currentProductMatches) {
    return (
      <>
        <Grid container spacing={2}>
          <Grid item xs={12} md={12} lg={4}>
            <UserCard />
            <InstructionsDialog />
            <CurrentProduct
              product={currentProduct}
              handleNextProduct={handleNextProduct}
            />
          </Grid>
          <Grid item md={12} md={12} lg={8}>
            <MatchingProducts products={currentProductMatches} />
          </Grid>
        </Grid>
      </>
    )
  } else {
    return <p>Loading...</p>
  }
}

export default LabelApp
