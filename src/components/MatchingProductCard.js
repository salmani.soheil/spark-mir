import React from "react"
import BottomNavigation from "@material-ui/core/BottomNavigation"
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction"
import { css } from "@emotion/react"
import { makeStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import CardHeader from "@material-ui/core/CardHeader"
import CardMedia from "@material-ui/core/CardMedia"
import Divider from "@material-ui/core/Divider"
import DragHandleIcon from "@material-ui/icons/DragHandle"
import QueueIcon from "@material-ui/icons/Queue"
import HelpIcon from "@material-ui/icons/Help"
import SimilarityPercentageBadge from "../components/SimilarityPercentageBadge"

const useStyles = makeStyles({
  card: {
    boxSizing: "border-box",
    height: "100%",
    flexDirection: "column",
    display: "flex",
  },
  cardActions: {
    backgroundColor: "rgba(0, 0, 0, 0.06)",
  },
})

const MatchingProductCard = ({ product }) => {
  const classes = useStyles()

  const [value, setValue] = React.useState("nothing")

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  return (
    <Card className={classes.card} elevation={5}>
      <CardHeader
        avatar={
          <SimilarityPercentageBadge
            similarityScore={product.similarityScore}
          />
        }
        title={product.description}
        subheader={product.packaging}
      />
      <CardMedia
        css={css`
          height: 300px;
          background-size: contain;
          margin: 1rem 0 1rem 0;
          flex-grow: 1;
        `}
        image={product.photos[0]}
      />
      <Divider />
      <BottomNavigation
        className={classes.cardActions}
        value={value}
        onChange={handleChange}
      >
        <BottomNavigationAction
          label="Substituable"
          value="substitute"
          icon={<DragHandleIcon />}
        />
        <BottomNavigationAction
          label="Complémentaire"
          value="complementary"
          icon={<QueueIcon />}
        />
        <BottomNavigationAction
          label="Aucun"
          value="nothing"
          icon={<HelpIcon />}
        />
      </BottomNavigation>
    </Card>
  )
}

export default MatchingProductCard
