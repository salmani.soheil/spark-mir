import React from "react"
import Card from "@material-ui/core/Card"
import Button from "@material-ui/core/Button"
import CardMedia from "@material-ui/core/CardMedia"
import CardHeader from "@material-ui/core/CardHeader"
import CardActions from "@material-ui/core/CardActions"
import Divider from "@material-ui/core/Divider"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
  card: {
    padding: "1rem",
  },
  cardHeader: {
    textAlign: "center",
    paddingTop: 0,
    "& span": {
      fontSize: "1rem",
    },
  },
  cardContent: {
    "& li": {
      padding: "0rem",
      margin: "0rem",
    },
  },
  cardMedia: {
    height: "300px",
    backgroundSize: "contain",
    margin: "1rem 0 1rem 0",
  },
  cardActions: {
    marginTop: "1rem",

    "& button": {
      display: "block",
      margin: "auto",
      width: "100%",
      backgroundColor: "rgb(0, 114, 0)",

      "&:hover": {
        backgroundColor: "#006400",
      },
    },
  },
})

function CurrentProduct({ product, handleNextProduct }) {
  const classes = useStyles()

  return (
    <Card className={classes.card} elevation={5}>
      <CardHeader
        className={classes.cardHeader}
        title={product.description}
        subheader={product.packaging}
      />
      <CardMedia className={classes.cardMedia} image={product.photos[0]} />
      <Divider />
      <CardActions className={classes.cardActions}>
        <Button variant="contained" color="primary" onClick={handleNextProduct}>
          Valider
        </Button>
      </CardActions>
    </Card>
  )
}

export default CurrentProduct
