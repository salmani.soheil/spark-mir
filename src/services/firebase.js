import { initializeApp } from "firebase/app"
import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth"
import { getFirestore, collection, doc, setDoc } from "firebase/firestore"

import firebaseConfig from "../../secrets/firebase.json"

initializeApp(firebaseConfig)

export const db = getFirestore()
export const auth = getAuth()
const googleProvider = new GoogleAuthProvider()
export const signInWithGoogle = () => {
  signInWithPopup(auth, googleProvider)
    .then(res => {
      const { displayName, email } = res.user
      const usersRef = collection(db, "users")
      setDoc(
        doc(usersRef, email),
        {
          displayName,
          email,
        },
        { merge: true }
      )
    })
    .catch(error => {
      console.log(error.message)
    })
}
export const signOut = () => {
  auth
    .signOut()
    .then(() => {
      console.log("Logged out...")
    })
    .catch(error => {
      console.log(error.message)
    })
}
