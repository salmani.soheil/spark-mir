import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import DialogActions from "@material-ui/core/DialogActions"
import DialogContent from "@material-ui/core/DialogContent"
import DialogContentText from "@material-ui/core/DialogContentText"
import DialogTitle from "@material-ui/core/DialogTitle"
import { makeStyles } from "@material-ui/core/styles"
import DragHandleIcon from "@material-ui/icons/DragHandle"
import HelpIcon from "@material-ui/icons/Help"
import QueueIcon from "@material-ui/icons/Queue"
import React from "react"

import complementarityImage from "../images/complementarity.png"
import noneImage from "../images/none.png"
import similarityImage from "../images/similarity.png"

const useStyles = makeStyles({
  dialogTitle: {
    textAlign: "center",
  },
  dialogButton: {
    width: "100%",
    marginBottom: 16,
    backgroundColor: "#777",
    "&:hover": {
      backgroundColor: "#666",
    },
  },
  image: {
    display: "block",
    width: 200,
    margin: "auto",
  },
})

export default function AlertDialog() {
  const [open, setOpen] = React.useState(true)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const classes = useStyles()

  return (
    <div>
      <Button
        className={classes.dialogButton}
        variant="contained"
        color="primary"
        onClick={handleClickOpen}
      >
        Besoin d'aide ?
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle className={classes.dialogTitle}>
          Rappel sur les règles de labélisation
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <p>
              <DragHandleIcon /> (similiarité)&nbsp;: signifie qu'en cas de
              rupture du produit courant, vous acheteriez l'autre produit.
            </p>
            <img src={similarityImage} className={classes.image} />
            <p>
              <QueueIcon /> (complémentarité)&nbsp;: signifie que vous
              acheteriez ce produit pour compléter le produit courant.
            </p>
            <img src={complementarityImage} className={classes.image} />
            <p>
              <HelpIcon /> (aucun)&nbsp;: signfie qu'il y a pas de lien de
              similiarité ni de complémentarité avec le produit courant.
            </p>
            <img src={noneImage} className={classes.image} />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Compris
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
