import { db } from "../services/firebase"
import { doc, setDoc, collection } from "firebase/firestore"

const updateProfileInfo = (user, data) => {
  const usersRef = collection(db, "users")
  setDoc(doc(usersRef, user.email), data, { merge: true })
}

export default updateProfileInfo
