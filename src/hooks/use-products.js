import { graphql, useStaticQuery } from "gatsby"

const useProducts = () => {
  const data = useStaticQuery(graphql`
    query {
      allProduct {
        nodes {
          barcode
          brand
          brandType
          description
          hypermarketClass
          hypermarketDepartment
          hypermarketSector
          hypermarketSubclass
          packaging
          photos
        }
      }
    }
  `)

  console.log(data.allProduct.nodes)
  return data.allProduct.nodes
}

export default useProducts
