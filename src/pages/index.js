import React, { useContext, useState } from "react"
import { makeStyles } from "@material-ui/core/styles"

import FirstConnectionForm from "../components/FirstConnectionForm"
import HomePresentation from "../components/HomePresentation"
import LabelApp from "../components/LabelApp"
import Layout from "../components/Layout"
import UserProvider, { UserContext } from "../providers/UserProvider"

function MainContent() {
  const user = useContext(UserContext)
  if (user && user.questions) {
    return <LabelApp />
  } else if (user) {
    return <FirstConnectionForm />
  } else {
    return <HomePresentation />
  }
}

function IndexPage() {
  return (
    <UserProvider>
      <Layout>
        <MainContent />
      </Layout>
    </UserProvider>
  )
}

export default IndexPage
