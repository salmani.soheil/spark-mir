import { getDoc } from "@firebase/firestore"
import React, { useState, useEffect, createContext } from "react"
import { auth, db } from "../services/firebase"
import { doc, onSnapshot } from "firebase/firestore"

export const UserContext = createContext({ user: null })

function UserProvider(props) {
  const [user, setUser] = useState(null)

  useEffect(() => {
    auth.onAuthStateChanged(async user => {
      if (user) {
        const unsub = onSnapshot(doc(db, "users", user.email), doc => {
          let userData = doc.data()
          let displayName = user.displayName
          let email = user.email
          let photoURL = user.photoURL
          let lastSignInTime = user.metadata.lastSignInTime
          let questions
          if ("questions" in userData) {
            questions = userData.questions
          } else {
            questions = null
          }
          setUser({
            displayName,
            email,
            photoURL,
            lastSignInTime,
            questions,
          })
        })
      } else {
        setUser(null)
      }
    })
  }, [])

  return (
    <UserContext.Provider value={user}>{props.children}</UserContext.Provider>
  )
}

export default UserProvider
