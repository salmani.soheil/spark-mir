import React, { useContext } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import Card from "@material-ui/core/Card"
import CardActions from "@material-ui/core/CardActions"
import CardHeader from "@material-ui/core/CardHeader"

import { signOut } from "../services/firebase"
import { UserContext } from "../providers/UserProvider"

const useStyles = makeStyles({
  card: {
    marginBottom: "16px",
  },
  cardActions: {
    justifyContent: "flex-end",
  },
})

function UserCard() {
  const classes = useStyles()

  const user = useContext(UserContext)
  const { displayName, photoURL } = user

  return (
    <Card className={classes.card} elevation={5}>
      <CardHeader
        avatar={<Avatar src={photoURL}></Avatar>}
        title={displayName}
        subheader="Connecté avec votre compte Google"
      />
      <CardActions className={classes.cardActions}>
        <Button onClick={signOut} color="primary">
          Déconnexion
        </Button>
      </CardActions>
    </Card>
  )
}

export default UserCard
