import React, { useEffect } from "react"
import Box from "@material-ui/core/Box"
import CircularProgress from "@material-ui/core/CircularProgress"
import Typography from "@material-ui/core/Typography"

function SimilarityPercentageBadge({ similarityScore }) {
  const [progress, setProgress] = React.useState(0)

  useEffect(() => {
    setProgress(similarityScore * 100)
  }, [similarityScore])

  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="determinate" value={progress} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          variant="caption"
          component="div"
          color="textSecondary"
        >{`${Math.round(progress)}%`}</Typography>
      </Box>
    </Box>
  )
}

export default SimilarityPercentageBadge
