import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import MatchingProductCard from "../components/MatchingProductCard"

const useStyles = makeStyles({
  grid: {
    alignItems: "stretch",
  },
})

const MatchingProducts = ({ products }) => {
  return (
    <Grid container spacing={2}>
      {products.map(product => (
        <Grid item xs={12} sm={6} md={4}>
          <MatchingProductCard
            key={`matching-product-${product.barcode}`}
            product={product}
          />
        </Grid>
      ))}
    </Grid>
  )
}

export default MatchingProducts
