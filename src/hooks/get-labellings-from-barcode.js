import { db } from "../services/firebase"
import { doc, setDoc, collection } from "firebase/firestore"

const saveLabellings = (user, data) => {
  const usersRef = collection(db, "labellings")
  setDoc(doc(usersRef, user.email), data, { merge: true })
}

export default saveLabellings
