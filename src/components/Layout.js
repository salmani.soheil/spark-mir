import React from "react"
import Helmet from "react-helmet"
import { makeStyles } from "@material-ui/core/styles"

import Header from "../components/Header"

const useStyles = makeStyles({
  main: {
    padding: "0 16px 16px 16px",
    minWidth: 540,
  },
})

function Layout({ children }) {
  const classes = useStyles()

  return (
    <>
      <Helmet>
        <html lang="fr" />
        <title>Carrefour LabelApp</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Cinzel+Decorative:wght@900&display=swap"
          rel="stylesheet"
        />
      </Helmet>
      <Header />
      <main className={classes.main}>{children}</main>
    </>
  )
}

export default Layout
