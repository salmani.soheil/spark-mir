import React, { useContext, useState } from "react"
import Form from "react-bootstrap/Form"
import "bootstrap/dist/css/bootstrap.min.css"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"
import { makeStyles } from "@material-ui/core"
import { UserContext } from "../providers/UserProvider"
import updateProfileInfo from "../hooks/update-profile-info"

const useStyles = makeStyles({
  card: {
    padding: "1rem",
    margin: "auto",
    maxWidth: 1080,
  },
  cardHeader: {
    textAlign: "center",
    paddingTop: 0,
    "& span": {
      fontSize: "1rem",
    },
  },
  cardTitle: {
    marginBottom: "1rem",
    textAlign: "center",
  },
  cardContent: {
    "& li": {
      padding: "0rem",
      margin: "0rem",
    },
  },
  formSubmit: {
    marginTop: "1rem",
    display: "block",
    width: "50%",
    margin: "auto",
  },
  form: {
    margin: "1rem 0 1rem 0",
  },
})

export default function DiscreteSlider() {
  let [numberPeopleHousehold, setNumberPeopleHousehold] = useState(1)
  let [numberChildrenHousehold, setNumberChildrenHousehold] = useState(0)
  let [productCategories, setProductCategories] = useState([])
  let [diets, setDiets] = useState([])
  let [buysReadyMeals, setBuysReadyMeals] = useState("false")
  let [cookingStyles, setCookingStyles] = useState([])
  const user = useContext(UserContext)

  const classes = useStyles()
  return (
    <Card className={classes.card} elevation={5}>
      <CardContent>
        <Typography
          className={classes.cardTitle}
          component="h2"
          variant="title"
          display="block"
        >
          Bienvenue !
        </Typography>
        <Typography variant="body2">
          Pour votre première connexion, remplissez ce formulaire afin que nous
          puissions adapter l'ensemble des produits à labéliser selon votre
          profil.
        </Typography>
        <Divider className={classes.form} />
        <Form>
          <Form.Group
            onChange={event => setNumberPeopleHousehold(event.target.value)}
          >
            <Form.Label>Nombre de personnes dans votre foyer</Form.Label>
            <Form.Control as="select">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10 ou +</option>
            </Form.Control>
          </Form.Group>
          <Form.Group
            onChange={event => setNumberChildrenHousehold(event.target.value)}
          >
            <Form.Label>Nombre d'enfants dans votre foyer</Form.Label>
            <Form.Control as="select" defaultValue={numberChildrenHousehold}>
              <option>0</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9 ou +</option>
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>
              Parmi les catégories de produits suivantes, lesquelles
              achetez-vous souvent ?
            </Form.Label>
            <Form.Control
              as="select"
              multiple
              onChange={event =>
                setProductCategories(
                  [].slice
                    .call(event.target.selectedOptions)
                    .map(item => item.value)
                )
              }
            >
              <option>Aide culinaire</option>
              <option>Aliments pour chats</option>
              <option>Desserts</option>
              <option>Entretien ligne</option>
              <option>Produits de rasage</option>
              <option>Pain industriel</option>
              <option>Pâte à tarte</option>
              <option>Pâtes</option>
            </Form.Control>
            <Form.Text className="text-muted">
              Maintenez la touche <kbd>Ctrl</kbd> et cliquez pour sélectionnez
              plusieurs choix.
            </Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Label>
              Parmi les régimes alimentaires suivants, lesquels suivez-vous ?
            </Form.Label>
            <Form.Control
              as="select"
              multiple
              onChange={event =>
                setDiets(
                  [].slice
                    .call(event.target.selectedOptions)
                    .map(item => item.value)
                )
              }
            >
              <option>Végétarien</option>
              <option>Végétalien</option>
              <option>Halal</option>
              <option>Sans porc</option>
              <option>Sans gluten</option>
              <option>Sans produits laitiers</option>
            </Form.Control>
            <Form.Text className="text-muted">
              Maintenez la touche <kbd>Ctrl</kbd> et cliquez pour sélectionnez
              plusieurs choix.
            </Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Label>Achetez-vous souvent des plats préparés ?</Form.Label>
            <Form.Control
              as="select"
              onChange={event => setBuysReadyMeals(event.target.value)}
            >
              <option value={true}>Oui</option>
              <option value={false}>Non</option>
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>
              Parmi les styles culinaires suivants, lesquels vous sont familiers
              ?
            </Form.Label>
            <Form.Control
              as="select"
              multiple
              onChange={event =>
                setCookingStyles(
                  [].slice
                    .call(event.target.selectedOptions)
                    .map(item => item.value)
                )
              }
            >
              <option>Cuisine africaine</option>
              <option>Cuisine chinoise</option>
              <option>Cuisine française</option>
              <option>Cuisine grecque</option>
              <option>Cuisine indienne</option>
              <option>Cuisine italienne</option>
              <option>Cuisine japonaise</option>
              <option>Cuisine libanaise</option>
              <option>Cuisine maghrébine</option>
              <option>Cuisine taïwanaise</option>
              <option>Cuisine turque</option>
            </Form.Control>
            <Form.Text className="text-muted">
              Maintenez la touche <kbd>Ctrl</kbd> et cliquez pour sélectionnez
              plusieurs choix.
            </Form.Text>
          </Form.Group>
          <Divider />
          <Button
            className={classes.formSubmit}
            variant="contained"
            color="primary"
            onClick={event => {
              event.preventDefault()
              updateProfileInfo(user, {
                questions: {
                  numberPeopleHousehold,
                  numberChildrenHousehold,
                  productCategories,
                  diets,
                  buysReadyMeals,
                  cookingStyles,
                },
              })
            }}
          >
            Confirmer
          </Button>
        </Form>
      </CardContent>
    </Card>
  )
}
