import { graphql, useStaticQuery } from "gatsby"

const useProductMatches = () => {
  const data = useStaticQuery(graphql`
    query {
      allProductMatches {
        nodes {
          barcode
          matches {
            barcode
            similarityScore
          }
        }
      }
    }
  `)

  return data.allProductMatches.nodes
}

export default useProductMatches
