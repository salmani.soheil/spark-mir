module.exports = {
  siteMetadata: {
    title: `LabelApp`,
    description: `TODO`,
    author: `Soheil SALMANI (soheil_salmani@carrefour.com)`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-material-ui`,
      options: {
        stylesProvider: {
          injectFirst: true,
        },
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-firestore`,
      options: {
        credential: require("./secrets/serviceAccount.json"),
        types: [
          {
            type: "Product",
            collection: "products",
            map: doc => ({
              barcode: doc.barcode,
              brand: doc.brand,
              brandType: doc.brandType,
              description: doc.description,
              hypermarketClass: doc.hypermarketClass,
              hypermarketDepartment: doc.hypermarketDepartment,
              hypermarketSector: doc.hypermarketSector,
              hypermarketSubclass: doc.hypermarketSubclass,
              packaging: doc.packaging,
              photos: doc.photos,
            }),
          },
          {
            type: "ProductMatches",
            collection: "productMatches",
            map: doc => ({
              barcode: doc.barcode,
              matches: doc.matches,
            }),
          },
        ],
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
